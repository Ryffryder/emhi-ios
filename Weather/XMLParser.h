

#import <Foundation/Foundation.h>
#import "Warnings.h"

@interface XMLParser : NSObject <NSXMLParserDelegate>

@property (strong, readonly) NSMutableArray *warnings;

-(id) loadXMLByURL:(NSString *)urlString;

@end
