

#import <UIKit/UIKit.h>
#define kCityComponent  0
#define kTypeComponent  1

@interface GraphicsViewController : UIViewController

<UIPickerViewDelegate, UIPickerViewDataSource>
{
    NSArray *cities;
}

@property (strong, nonatomic) IBOutlet UIPickerView *doublePicker;
@property (nonatomic, strong)  NSArray *cities;
@property (nonatomic,strong)  NSArray *typeOfGraphics;

- (IBAction)buttonPressed:(id)sender;
- (IBAction)backGraphics:(id)sender;

@end
