
#import "AppDelegate.h"
#import "FirstViewController.h"
#import "SecondViewController.h"
#import "FourthViewController.h"
#import "AboutViewController.h"

@implementation AppDelegate
@synthesize window = _window;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [TestFlight takeOff:@"b7f0708c3c532e86b9e4455a6dcc0c76_MTUyMDY0MjAxMi0xMS0wNiAxMjoxODoxMy41OTUxNDQ"];
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    self.window.backgroundColor = [UIColor whiteColor];
    FourthViewController *myMapViewController = [[FourthViewController alloc]
                                              initWithNibName:@"FourthViewController"
                                              bundle:[NSBundle mainBundle]];
    
    
    UINavigationController *nav = [[UINavigationController alloc]
                                   initWithRootViewController:myMapViewController];
    [[self window] addSubview:[nav view]];
    UIViewController *viewController1, *viewController2, *viewController3, *viewController4;
    
    viewController1 = [[FirstViewController alloc]
                        initWithNibName:@"FirstViewController" bundle:nil];
    UINavigationController *navController1 = [[UINavigationController alloc]
                                               initWithRootViewController:viewController1];
    viewController2 = [[SecondViewController alloc]
                        initWithNibName:@"SecondViewController" bundle:nil];
    UINavigationController *navController2 = [[UINavigationController alloc]
                                               initWithRootViewController:viewController2];
    viewController3 = [[FourthViewController alloc]
                       initWithNibName:@"FourthViewController" bundle:nil];
    UINavigationController *navController3 = [[UINavigationController alloc]
                                              initWithRootViewController:viewController3];
    viewController4 = [[AboutViewController alloc]
                       initWithNibName:@"AboutViewController" bundle:nil];
    UINavigationController *navController4 = [[UINavigationController alloc]
                                              initWithRootViewController:viewController4];
    
    
    _tabBarController = [[UITabBarController alloc] init];
    self.tabBarController.viewControllers = [NSArray arrayWithObjects:navController1, navController2, navController3,navController4, nil];
    self.window.rootViewController = self.tabBarController;
    navController1.navigationBar.tintColor = [UIColor blackColor];
    navController2.navigationBar.tintColor = [UIColor blackColor];
    navController3.navigationBar.tintColor = [UIColor blackColor];
    navController4.navigationBar.tintColor = [UIColor blackColor];
    
    self.navigationItem.leftBarButtonItem  = [[UIBarButtonItem alloc] initWithTitle:@"Minimum" style:UIBarButtonItemStyleBordered target:self action:@selector(defaultsMinimumAction:)];
    [self.window makeKeyAndVisible];
    
    return YES;

}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

/*
// Optional UITabBarControllerDelegate method.
- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController
{
}
*/

/*
// Optional UITabBarControllerDelegate method.
- (void)tabBarController:(UITabBarController *)tabBarController didEndCustomizingViewControllers:(NSArray *)viewControllers changed:(BOOL)changed
{
}
*/

@end
