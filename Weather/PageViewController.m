

#import "PageViewController.h"
#import "DataSource.h"
#import <QuartzCore/QuartzCore.h>

const CGFloat TEXT_VIEW_PADDING = 40.0;

@implementation PageViewController

@synthesize pageIndex;


- (void)setPageIndex:(NSInteger)newPageIndex
{
	pageIndex = newPageIndex;
	
	if (pageIndex >= 0 && pageIndex < [[DataSource sharedDataSource] numDataPages])
	{
		NSDictionary *pageData =
        [[DataSource sharedDataSource] dataForPage:pageIndex];
		label.text = [pageData objectForKey:@"dateText"];
        textView.text = [pageData objectForKey:@"dayText"];
        textView2.text = [pageData objectForKey:@"nightText"];
		
		CGRect absoluteRect = [self.view.window
                               convertRect:textView.bounds
                               fromView:textView];
		if (!self.view.window ||
			!CGRectIntersectsRect(
                                  CGRectInset(absoluteRect, TEXT_VIEW_PADDING, TEXT_VIEW_PADDING),
                                  [self.view.window bounds]))
		{
			textViewNeedsUpdate = YES;
		}
        
        CGRect absoluteRect2 = [self.view.window
                               convertRect:textView2.bounds
                               fromView:textView2];
		if (!self.view.window ||
			!CGRectIntersectsRect(
                                  CGRectInset(absoluteRect2, TEXT_VIEW_PADDING, TEXT_VIEW_PADDING),
                                  [self.view.window bounds]))
		{
			textViewNeedsUpdate = YES;
		}
	}
}

- (void)updateTextViews:(BOOL)force
{
	if (force ||
		(textViewNeedsUpdate &&
         self.view.window &&
         CGRectIntersectsRect(
                              [self.view.window
                               convertRect:CGRectInset(textView.bounds, TEXT_VIEW_PADDING, TEXT_VIEW_PADDING)
                               fromView:textView],
                              [self.view.window bounds])))
	{
		for (UIView *childView in textView.subviews)
		{
			[childView setNeedsDisplay];
		}
		textViewNeedsUpdate = NO;
	}
}

- (void)updateTextViews2:(BOOL)force
{
	if (force ||
		(textViewNeedsUpdate &&
         self.view.window &&
         CGRectIntersectsRect(
                              [self.view.window
                               convertRect:CGRectInset(textView2.bounds, TEXT_VIEW_PADDING, TEXT_VIEW_PADDING)
                               fromView:textView2],
                              [self.view.window bounds])))
	{
		for (UIView *childView in textView2.subviews)
		{
			[childView setNeedsDisplay];
		}
		textViewNeedsUpdate = NO;
	}
}

@end
