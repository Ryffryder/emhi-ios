

#import "WarningsViewController.h"
#import "XMLParser.h"
#import "Autosize.h"

@interface WarningsViewController ()

@end


@implementation WarningsViewController
@synthesize customImage = _customImage;
@synthesize warningsTableView;

XMLParser *xmlParser;
CGRect areaFrame;
UILabel *areaLabel;
CGRect contentFrame;
UILabel *contentLabel;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([[xmlParser warnings] count]<1) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Attention"
                                                       message:@"There are no warnings given by EMHI at the moment. You can try refresh later!"
                                                      delegate:self
                                             cancelButtonTitle:@"Close"
                                             otherButtonTitles:nil];
        [alert show];
    }
    
    return [[xmlParser warnings] count];
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier = @"Cell";
	Warnings *currentWarnings = [[xmlParser warnings] objectAtIndex:indexPath.row];
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        
        CGRect contentFrame = CGRectMake(10, 10, 280, 70);
        UILabel *contentLabel = [[UILabel alloc] initWithFrame:contentFrame];
        contentLabel.tag = 0011;
        contentLabel.numberOfLines = 8;
        contentLabel.font = [UIFont italicSystemFontOfSize:14];
        [cell.contentView addSubview:contentLabel];
        
        CGRect areaFrame = CGRectMake(10, 80, 280, 12);
        UILabel *areaLabel = [[UILabel alloc] initWithFrame:areaFrame];
        areaLabel.tag = 0012;
        areaLabel.numberOfLines = 1;
        areaLabel.font = [UIFont boldSystemFontOfSize:13];
        [cell.contentView addSubview:areaLabel];
    }
	
	UILabel *contentLabel = (UILabel *)[cell.contentView viewWithTag:0011];
    contentLabel.text = [currentWarnings content];
	
    
    
	UILabel *areaLabel = (UILabel *)[cell.contentView viewWithTag:0012];
    areaLabel.text = [currentWarnings warnLocation];
    
    UIImageView *tempImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Layer 2.png"]];
    [tempImageView setFrame:self.warningsTableView.frame];
    
    self.warningsTableView.backgroundView = tempImageView;
	
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	return 100;
}

#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}


#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    xmlParser = [[XMLParser alloc] loadXMLByURL:@"http://www.emhi.ee/ilma_andmed/xml/hoiatus.php"];
    
    self.title = @"Warnings";
}


- (void)viewDidUnload
{
    [self setWarningsTableView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}



- (IBAction)backWarnings:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}


@end
