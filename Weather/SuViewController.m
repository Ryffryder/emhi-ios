
#import "SuViewController.h"

@interface SuViewController ()

@end

static int currentIndex = 0;

@implementation SuViewController

@synthesize imageView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    NSURL *imageURL01 = [NSURL URLWithString:@"http://www.emhi.ee/ilma_andmed/radar/SUR_CAP_01.gif"];
    NSData *imageData01 = [NSData dataWithContentsOfURL:imageURL01];
    UIImage *image01 = [UIImage imageWithData:imageData01];
    NSURL *imageURL02 = [NSURL URLWithString:@"http://www.emhi.ee/ilma_andmed/radar/SUR_CAP_02.gif"];
    NSData *imageData02 = [NSData dataWithContentsOfURL:imageURL02];
    UIImage *image02 = [UIImage imageWithData:imageData02];
    NSURL *imageURL03 = [NSURL URLWithString:@"http://www.emhi.ee/ilma_andmed/radar/SUR_CAP_03.gif"];
    NSData *imageData03 = [NSData dataWithContentsOfURL:imageURL03];
    UIImage *image03 = [UIImage imageWithData:imageData03];
    NSURL *imageURL04 = [NSURL URLWithString:@"http://www.emhi.ee/ilma_andmed/radar/SUR_CAP_04.gif"];
    NSData *imageData04 = [NSData dataWithContentsOfURL:imageURL04];
    UIImage *image04 = [UIImage imageWithData:imageData04];
    NSURL *imageURL05 = [NSURL URLWithString:@"http://www.emhi.ee/ilma_andmed/radar/SUR_CAP_05.gif"];
    NSData *imageData05 = [NSData dataWithContentsOfURL:imageURL05];
    UIImage *image05 = [UIImage imageWithData:imageData05];
    NSURL *imageURL06 = [NSURL URLWithString:@"http://www.emhi.ee/ilma_andmed/radar/SUR_CAP_06.gif"];
    NSData *imageData06 = [NSData dataWithContentsOfURL:imageURL06];
    UIImage *image06 = [UIImage imageWithData:imageData06];
    NSURL *imageURL07 = [NSURL URLWithString:@"http://www.emhi.ee/ilma_andmed/radar/SUR_CAP_07.gif"];
    NSData *imageData07 = [NSData dataWithContentsOfURL:imageURL07];
    UIImage *image07 = [UIImage imageWithData:imageData07];
    NSURL *imageURL08 = [NSURL URLWithString:@"http://www.emhi.ee/ilma_andmed/radar/SUR_CAP_08.gif"];
    NSData *imageData08 = [NSData dataWithContentsOfURL:imageURL08];
    UIImage *image08 = [UIImage imageWithData:imageData08];
    NSURL *imageURL09 = [NSURL URLWithString:@"http://www.emhi.ee/ilma_andmed/radar/SUR_CAP_09.gif"];
    NSData *imageData09 = [NSData dataWithContentsOfURL:imageURL09];
    UIImage *image09 = [UIImage imageWithData:imageData09];
    NSURL *imageURL10 = [NSURL URLWithString:@"http://www.emhi.ee/ilma_andmed/radar/SUR_CAP_10.gif"];
    NSData *imageData10 = [NSData dataWithContentsOfURL:imageURL10];
    UIImage *image10 = [UIImage imageWithData:imageData10];
    NSURL *imageURL11 = [NSURL URLWithString:@"http://www.emhi.ee/ilma_andmed/radar/SUR_CAP_11.gif"];
    NSData *imageData11 = [NSData dataWithContentsOfURL:imageURL11];
    UIImage *image11 = [UIImage imageWithData:imageData11];
    NSURL *imageURL12 = [NSURL URLWithString:@"http://www.emhi.ee/ilma_andmed/radar/SUR_CAP_12.gif"];
    NSData *imageData12 = [NSData dataWithContentsOfURL:imageURL12];
    UIImage *image12 = [UIImage imageWithData:imageData12];
    
    NSArray *images = [NSArray arrayWithObjects:image01,image02,image03,
                       image04,image05,image06,
                       image07,image08,image09,
                       image10,image11,image12, nil];
    
    [imageView setAnimationImages:images];
    imageView.animationDuration = 3.50;
    imageView.animationRepeatCount = 0;
    imageView.image = image01;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (IBAction)backButton:(id)sender {
    if (currentIndex != 0) {
        currentIndex -= 1;
        imageView.image = [imageView.animationImages objectAtIndex:currentIndex];
    } else {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert"
                                                       message:@"This is the first image"
                                                      delegate:self
                                             cancelButtonTitle:@"Close"
                                             otherButtonTitles:nil];
        [alert show];
    }
}

- (IBAction)playButton:(id)sender {
    if (imageView.isAnimating == TRUE) {
        [imageView stopAnimating];
        imageView.image = [imageView.animationImages objectAtIndex:currentIndex];
    } else {
        [imageView startAnimating];
    }
}

- (IBAction)nextButton:(id)sender {
    if (currentIndex != 11) {
        currentIndex += 1;
        imageView.image = [imageView.animationImages objectAtIndex:currentIndex];
    } else {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert"
                                                       message:@"This is the last image"
                                                      delegate:self
                                             cancelButtonTitle:@"Close"
                                             otherButtonTitles:nil];
        [alert show];
    }
}

- (IBAction)toMenu:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
