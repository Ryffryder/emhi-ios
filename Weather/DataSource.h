
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "DDXML.h"

@interface DataSource : NSObject
{
	NSArray *dataPages;
}

+ (DataSource *)sharedDataSource;
- (NSInteger)numDataPages;
- (NSDictionary *)dataForPage:(NSInteger)pageIndex;

@end