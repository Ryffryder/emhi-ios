
#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>

@interface FourthViewController : UIViewController <CLLocationManagerDelegate> {
    CLLocationManager *locationManager;
}
@property (retain, nonatomic) IBOutlet MKMapView *myMapView;

@property (retain, nonatomic) CLLocationManager *locationManager;

@end
