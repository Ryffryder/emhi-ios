
#import "Hoiatused.h"

@implementation Hoiatused
@synthesize timestamp, area_est, content_est, warning;
- (void) dealloc
{
    [timestamp release];
    [area_est release];
    [content_est release];
    [super dealloc];
}
@end



