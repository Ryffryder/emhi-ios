
#import "ThirdViewController.h"

@interface ThirdViewController ()

@end

static int currentIndex = 0;

@implementation ThirdViewController

@synthesize imageView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    NSURL *imageURL01 = [NSURL URLWithString:@"http://www.emhi.ee/ilma_andmed/sat/ir108_12.jpg"];
    NSData *imageData01 = [NSData dataWithContentsOfURL:imageURL01];
    UIImage *image01 = [UIImage imageWithData:imageData01];
    NSURL *imageURL02 = [NSURL URLWithString:@"http://www.emhi.ee/ilma_andmed/sat/ir108_11.jpg"];
    NSData *imageData02 = [NSData dataWithContentsOfURL:imageURL02];
    UIImage *image02 = [UIImage imageWithData:imageData02];
    NSURL *imageURL03 = [NSURL URLWithString:@"http://www.emhi.ee/ilma_andmed/sat/ir108_10.jpg"];
    NSData *imageData03 = [NSData dataWithContentsOfURL:imageURL03];
    UIImage *image03 = [UIImage imageWithData:imageData03];
    NSURL *imageURL04 = [NSURL URLWithString:@"http://www.emhi.ee/ilma_andmed/sat/ir108_09.jpg"];
    NSData *imageData04 = [NSData dataWithContentsOfURL:imageURL04];
    UIImage *image04 = [UIImage imageWithData:imageData04];
    NSURL *imageURL05 = [NSURL URLWithString:@"http://www.emhi.ee/ilma_andmed/sat/ir108_08.jpg"];
    NSData *imageData05 = [NSData dataWithContentsOfURL:imageURL05];
    UIImage *image05 = [UIImage imageWithData:imageData05];
    NSURL *imageURL06 = [NSURL URLWithString:@"http://www.emhi.ee/ilma_andmed/sat/ir108_07.jpg"];
    NSData *imageData06 = [NSData dataWithContentsOfURL:imageURL06];
    UIImage *image06 = [UIImage imageWithData:imageData06];
    NSURL *imageURL07 = [NSURL URLWithString:@"http://www.emhi.ee/ilma_andmed/sat/ir108_06.jpg"];
    NSData *imageData07 = [NSData dataWithContentsOfURL:imageURL07];
    UIImage *image07 = [UIImage imageWithData:imageData07];
    NSURL *imageURL08 = [NSURL URLWithString:@"http://www.emhi.ee/ilma_andmed/sat/ir108_05.jpg"];
    NSData *imageData08 = [NSData dataWithContentsOfURL:imageURL08];
    UIImage *image08 = [UIImage imageWithData:imageData08];
    NSURL *imageURL09 = [NSURL URLWithString:@"http://www.emhi.ee/ilma_andmed/sat/ir108_04.jpg"];
    NSData *imageData09 = [NSData dataWithContentsOfURL:imageURL09];
    UIImage *image09 = [UIImage imageWithData:imageData09];
    NSURL *imageURL10 = [NSURL URLWithString:@"http://www.emhi.ee/ilma_andmed/sat/ir108_03.jpg"];
    NSData *imageData10 = [NSData dataWithContentsOfURL:imageURL10];
    UIImage *image10 = [UIImage imageWithData:imageData10];
    NSURL *imageURL11 = [NSURL URLWithString:@"http://www.emhi.ee/ilma_andmed/sat/ir108_02.jpg"];
    NSData *imageData11 = [NSData dataWithContentsOfURL:imageURL11];
    UIImage *image11 = [UIImage imageWithData:imageData11];
    NSURL *imageURL12 = [NSURL URLWithString:@"http://www.emhi.ee/ilma_andmed/sat/ir108_01.jpg"];
    NSData *imageData12 = [NSData dataWithContentsOfURL:imageURL12];
    UIImage *image12 = [UIImage imageWithData:imageData12];
    
    NSArray *images = [NSArray arrayWithObjects:image01,image02,image03,
                                                image04,image05,image06,
                                                image07,image08,image09,
                                                image10,image11,image12, nil];
    
    [imageView setAnimationImages:images];
    imageView.animationDuration = 3.50;
    imageView.animationRepeatCount = 0;
    imageView.image = image01;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (IBAction)backButton:(id)sender {
    if (currentIndex != 0) {
        currentIndex -= 1;
        imageView.image = [imageView.animationImages objectAtIndex:currentIndex];
    } else {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert"
                                                       message:@"This is the first image"
                                                      delegate:self
                                             cancelButtonTitle:@"Close"
                                             otherButtonTitles:nil];
        [alert show];
    }
}

- (IBAction)playButton:(id)sender {
    if (imageView.isAnimating == TRUE) {
        [imageView stopAnimating];
        imageView.image = [imageView.animationImages objectAtIndex:currentIndex];
    } else {
        [imageView startAnimating];
    }
}

- (IBAction)nextButton:(id)sender {
    if (currentIndex != 11) {
        currentIndex += 1;
        imageView.image = [imageView.animationImages objectAtIndex:currentIndex];
    } else {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert"
                                                       message:@"This is the last image"
                                                      delegate:self
                                             cancelButtonTitle:@"Close"
                                             otherButtonTitles:nil];
        [alert show];
    }
}

- (IBAction)toMenu:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
