
#import <UIKit/UIKit.h>

@interface PageViewController : UIViewController
{
	NSInteger pageIndex;
	BOOL textViewNeedsUpdate;
	IBOutlet UILabel *label;
	IBOutlet UITextView *textView;
    IBOutlet UITextView *textView2;

}

@property (nonatomic) NSInteger pageIndex;

- (void)updateTextViews:(BOOL)force;
- (void)updateTextViews2:(BOOL)force;

@end