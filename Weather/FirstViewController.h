
#import <UIKit/UIKit.h>
#import "DDXML.h"

@class PageViewController;
@interface FirstViewController : UIViewController  {
    
    IBOutlet UIScrollView *scrollView;
	IBOutlet UIPageControl *pageControl;
	
	PageViewController *currentPage;
	PageViewController *nextPage;
}

- (IBAction)changePage:(id)sender;

@end