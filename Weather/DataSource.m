
#import "DataSource.h"
#import "SynthesizeSingleton.h"
#import "DDXMLElement.h"

@implementation DataSource

SYNTHESIZE_SINGLETON_FOR_CLASS(DataSource);

- (id)init
{
    

    //SEE OSA *************************************************************************************//
    //SEE OSA *************************************************************************************//
/*    NSMutableArray *places = [NSMutableArray arrayWithCapacity:40];
    NSMutableArray *phenomenon = [NSMutableArray arrayWithCapacity:40];
    NSMutableArray *temperature = [NSMutableArray arrayWithCapacity:40];
    NSMutableArray *wind = [NSMutableArray arrayWithCapacity:40];
    NSMutableArray *humidity = [NSMutableArray arrayWithCapacity:40];
    
    
    NSString *observationsData = [NSString stringWithContentsOfURL:[NSURL URLWithString:@"http://www.emhi.ee/ilma_andmed/xml/observations.php"] encoding:NSISOLatin1StringEncoding error:nil];
    NSData *dataFromXml = [observationsData dataUsingEncoding:NSUTF8StringEncoding];
    DDXMLDocument *xmlObservationsDoc = [[DDXMLDocument alloc] initWithData:dataFromXml options:0 error:nil];
    
    NSArray *getPlaces = [xmlObservationsDoc nodesForXPath:@"//station" error:nil];
    for(DDXMLElement *el in getPlaces){
        NSString *tt = [[[el elementsForName:@"name"] lastObject] stringValue];
        
        char converted[([tt length] +1)];
        [tt getCString:converted maxLength:([tt length]+1) encoding:NSISOLatin1StringEncoding];
        NSString *converted_str = [NSString stringWithCString:converted encoding:NSUTF8StringEncoding];
        
        [places addObject:[NSString stringWithFormat:@"%@", converted_str]];
    }
    
    NSArray *getPhenomenon = [xmlObservationsDoc nodesForXPath:@"//station" error:nil];
    for(DDXMLElement *el in getPhenomenon){
        NSString *tt = [[[el elementsForName:@"phenomenon"] lastObject] stringValue];
        
        char converted[([tt length] +1)];
        [tt getCString:converted maxLength:([tt length]+1) encoding:NSISOLatin1StringEncoding];
        NSString *converted_str = [NSString stringWithCString:converted encoding:NSUTF8StringEncoding];
        
        [phenomenon addObject:[NSString stringWithFormat:@"%@", converted_str]];
    }
    
    NSArray *getTemperature = [xmlObservationsDoc nodesForXPath:@"//station" error:nil];
    for(DDXMLElement *el in getTemperature){
        NSString *tt = [[[el elementsForName:@"airtemperature"] lastObject] stringValue];
        
        char converted[([tt length] +1)];
        [tt getCString:converted maxLength:([tt length]+1) encoding:NSISOLatin1StringEncoding];
        NSString *converted_str = [NSString stringWithCString:converted encoding:NSUTF8StringEncoding];
        
        [temperature addObject:[NSString stringWithFormat:@"%@", converted_str]];
    }
    
    NSArray *getWind = [xmlObservationsDoc nodesForXPath:@"//station" error:nil];
    for(DDXMLElement *el in getWind){
        NSString *tt = [[[el elementsForName:@"windspeed"] lastObject] stringValue];
        
        char converted[([tt length] +1)];
        [tt getCString:converted maxLength:([tt length]+1) encoding:NSISOLatin1StringEncoding];
        NSString *converted_str = [NSString stringWithCString:converted encoding:NSUTF8StringEncoding];
        
        [wind addObject:[NSString stringWithFormat:@"%@", converted_str]];
    }
    
    NSArray *gethumidity = [xmlObservationsDoc nodesForXPath:@"//station" error:nil];
    for(DDXMLElement *el in gethumidity){
        NSString *tt = [[[el elementsForName:@"relativehumidity"] lastObject] stringValue];
        
        char converted[([tt length] +1)];
        [tt getCString:converted maxLength:([tt length]+1) encoding:NSISOLatin1StringEncoding];
        NSString *converted_str = [NSString stringWithCString:converted encoding:NSUTF8StringEncoding];
        
        [humidity addObject:[NSString stringWithFormat:@"%@", converted_str]];
    }
    
    NSLog([places description]);
    NSLog([phenomenon description]);
    NSLog([temperature description]);
    NSLog([wind description]);
    NSLog([humidity description]);*/
//SEE OSA *************************************************************************************//
//SEE OSA *************************************************************************************//
    
    
    
    NSMutableArray *daydata = [NSMutableArray arrayWithCapacity:4];
    NSMutableArray *nightdata = [NSMutableArray arrayWithCapacity:4];
    NSMutableArray *dates = [NSMutableArray arrayWithCapacity:4];
    

    
    NSString *date = [NSString stringWithContentsOfURL:[NSURL URLWithString:@"http://www.emhi.ee/ilma_andmed/xml/forecast.php?lang=eng"] encoding:NSISOLatin1StringEncoding error:nil];
    NSData *xmlData = [date dataUsingEncoding:NSUTF8StringEncoding];
    DDXMLDocument *xmlDoc = [[DDXMLDocument alloc] initWithData:xmlData options:0 error:nil];
    NSArray *resultNodes = nil;
    resultNodes = [xmlDoc nodesForXPath:@"//forecast" error:nil];
    for(DDXMLElement* resultElement in resultNodes){
        NSString *temp = [[resultElement attributeForName:@"date"] stringValue];
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
        NSDate *date = [dateFormatter dateFromString:temp];
        
        [dateFormatter setDateFormat:@"EEEE dd.MMM, yyyy"];
        [dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:3600*2]];
        NSString *formattedDate = [dateFormatter stringFromDate:date];
        
        [dates addObject:[NSString stringWithFormat:@"%@", formattedDate]];
    }
    //    NSLog([dates description]);
    NSArray *getNight = [xmlDoc nodesForXPath:@"//night" error:nil];
    for(DDXMLElement *el in getNight){
        NSString *tt = [[[el elementsForName:@"text"] lastObject] stringValue];
        
        char converted[([tt length] +1)];
        [tt getCString:converted maxLength:([tt length]+1) encoding:NSISOLatin1StringEncoding];
        NSString *converted_str = [NSString stringWithCString:converted encoding:NSUTF8StringEncoding];
        
        [nightdata addObject:[NSString stringWithFormat:@"%@", converted_str]];
    }
    NSArray *getDay = [xmlDoc nodesForXPath:@"//day" error:nil];
    for(DDXMLElement *el in getDay){
        NSString *day = [[[el elementsForName:@"text"] lastObject] stringValue];
        
        char converted[([day length] +1)];
        [day getCString:converted maxLength:([day length]+1) encoding:NSISOLatin1StringEncoding];
        NSString *converted_str = [NSString stringWithCString:converted encoding:NSUTF8StringEncoding];
        
        [daydata addObject:[NSString stringWithFormat:@"%@", converted_str]];
    }
    
    
	self = [super init];
	if (self != nil)
	{
		dataPages = [[NSArray alloc] initWithObjects:
                     [NSDictionary dictionaryWithObjectsAndKeys:
                      [dates objectAtIndex:0], @"dateText",
                      [daydata objectAtIndex:0], @"dayText",
                      [nightdata objectAtIndex:0], @"nightText",
                      nil],
                     [NSDictionary dictionaryWithObjectsAndKeys:
                      [dates objectAtIndex:1], @"dateText",
                      [daydata objectAtIndex:1], @"dayText",
                      [nightdata objectAtIndex:1], @"nightText",
                      nil],
                     [NSDictionary dictionaryWithObjectsAndKeys:
                      [dates objectAtIndex:2], @"dateText",
                      [daydata objectAtIndex:2], @"dayText",
                      [nightdata objectAtIndex:2], @"nightText",
                      nil],
                     [NSDictionary dictionaryWithObjectsAndKeys:
                      [dates objectAtIndex:3], @"dateText",
                      [daydata objectAtIndex:3], @"dayText",
                      [nightdata objectAtIndex:3], @"nightText",
                      nil],
                     nil];
	}
	return self;
}

- (NSInteger)numDataPages
{
	return [dataPages count];
}

- (NSDictionary *)dataForPage:(NSInteger)pageIndex
{
	return [dataPages objectAtIndex:pageIndex];
}

@end
