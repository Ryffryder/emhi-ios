
#import "SecondViewController.h"
#import "GraphicsViewController.h"
#import "WarningsViewController.h"
#import "FireHazardViewController.h"
#import "SuViewController.h"
#import "ThirdViewController.h"
#import "HaViewController.h"

@interface SecondViewController ()

@end

@implementation SecondViewController

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    return nil;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = NSLocalizedString(@"Observations", @"Second");
        self.tabBarItem.image = [UIImage imageNamed:@"uptrend"];
    }
    return self;
}
							
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

- (IBAction)toPrognose:(id)sender {
    GraphicsViewController *pv = [[GraphicsViewController alloc]init];
    [self presentViewController:pv animated:YES completion:nil];
}

- (IBAction)toWarnings:(id)sender {
    WarningsViewController *wv = [[WarningsViewController alloc]init];
    [self presentViewController:wv animated:YES completion:nil];
}

- (IBAction)toFire:(id)sender {
    FireHazardViewController *wv = [[FireHazardViewController alloc]init];
    [self presentViewController:wv animated:YES completion:nil];
}

- (IBAction)toSugavere:(id)sender {
    SuViewController *wv = [[SuViewController alloc]init];
    [self presentViewController:wv animated:YES completion:nil];
}
    
- (IBAction)toHarku:(id)sender {
    HaViewController *wv = [[HaViewController alloc]init];
    [self presentViewController:wv animated:YES completion:nil];
}

- (IBAction)toSatellite:(id)sender {
    ThirdViewController *wv = [[ThirdViewController alloc]init];
    [self presentViewController:wv animated:YES completion:nil];
}


@end
