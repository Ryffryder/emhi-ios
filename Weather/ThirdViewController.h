
#import <UIKit/UIKit.h>

@interface ThirdViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIImageView *imageView;

- (IBAction)backButton:(id)sender;
- (IBAction)playButton:(id)sender;
- (IBAction)nextButton:(id)sender;
- (IBAction)toMenu:(id)sender;

@end
