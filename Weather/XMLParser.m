


#import <Foundation/Foundation.h>
#import "XMLParser.h"

@implementation XMLParser
@synthesize warnings = _warnings;


NSMutableString	*currentNodeContent;
NSXMLParser		*parser;
Warnings		*currentWarnings;
bool            isStatus;

-(id) loadXMLByURL:(NSString *)urlString
{
	_warnings       = [[NSMutableArray alloc] init];
	NSURL *url		= [NSURL URLWithString:urlString];
	NSData	*data   = [[NSData alloc] initWithContentsOfURL:url];
	parser			= [[NSXMLParser alloc] initWithData:data];
	parser.delegate = self;
	[parser parse];
	return self;
}

- (void) parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
	currentNodeContent = (NSMutableString *) [string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

- (void) parser:(NSXMLParser *)parser didStartElement:(NSString *)elementname namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
	if ([elementname isEqualToString:@"area_eng"])
	{
		currentWarnings = [Warnings alloc];
        isStatus = YES;
	}
	if ([elementname isEqualToString:@"comntent_eng"])
	{
        isStatus = NO;
	}
}

- (void) parser:(NSXMLParser *)parser didEndElement:(NSString *)elementname namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    if (isStatus)
    {
        if ([elementname isEqualToString:@"area_eng"])
        {
            currentWarnings.warnLocation = currentNodeContent;
        }
        if ([elementname isEqualToString:@"content_eng"])
        {
            currentWarnings.content = currentNodeContent;
        }
    }
	if ([elementname isEqualToString:@"content_eng"])
	{
		[self.warnings addObject:currentWarnings];
		currentWarnings = nil;
		currentNodeContent = nil;
	}
}

@end