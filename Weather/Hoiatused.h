

#import <Foundation/Foundation.h>

@interface Hoiatused : NSObject {
    NSString *warning;
    NSString *timestamp;
    NSString *area_est;
    NSString *content_est;
    
}
@property (nonatomic, retain) NSString *warning;
@property (nonatomic, retain) NSString *timestamp;
@property (nonatomic, retain) NSString *area_est;
@property (nonatomic, retain) NSString *content_est;
@end

