

#import <UIKit/UIKit.h>
#import "XMLParser.h"
#import "Warnings.h"

@interface WarningsViewController : UIViewController
- (IBAction)backWarnings:(id)sender;

@property (nonatomic, strong) UIImageView *customImage;

@property (strong, nonatomic) IBOutlet UITableView *warningsTableView;


@end
