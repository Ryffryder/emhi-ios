
#import "GraphicsViewController.h"

@interface GraphicsViewController ()

@end

@implementation GraphicsViewController

@synthesize doublePicker;
@synthesize cities;
@synthesize typeOfGraphics;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    NSArray *citiesArray = [[NSArray alloc] initWithObjects: @"Haapsalu",@"Jõgeva",@"Jõhvi",@"Kihnu",@"Kunda",@"Kuusiku",@"Lääne-Nigula",@"Narva-Jõesuu",@"Pakri",@"Pärnu",@"Ristna",@"Ruhnu",@"Tallinn",@"Tartu",@"Tiirikoja",@"Türi",@"Valga",@"Viljandi",@"Vilsandi",@"Virtsu",@"Võru",@"Väike-Maarja", nil];
    
    NSArray *typeOfGraphicsArray = [[NSArray alloc] initWithObjects:
                             @"Wind",@"Tempepature",@"Humidity",nil];
    self.cities = citiesArray;
    self.typeOfGraphics = typeOfGraphicsArray;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 2;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (component == kCityComponent)
        return[self.cities count];
    return[self.typeOfGraphics count];
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if (component == kCityComponent)
        return [self.cities objectAtIndex:row];
    return [self.typeOfGraphics objectAtIndex:row];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)buttonPressed:(id)sender {
    NSInteger cityRow = [doublePicker selectedRowInComponent:kCityComponent];
    NSString *city = [cities objectAtIndex:cityRow];
    
    NSInteger typeRow = [doublePicker selectedRowInComponent:kTypeComponent];
    NSString *type = [typeOfGraphics objectAtIndex:typeRow];
    
    city = [city stringByReplacingOccurrencesOfString:@"ä" withString:@"a"];
    city = [city stringByReplacingOccurrencesOfString:@"õ" withString:@"o"];
    city = [city stringByReplacingOccurrencesOfString:@"ü" withString:@"u"];
    
    type = [type stringByReplacingOccurrencesOfString:@"Tempepature" withString:@"temp"];
    type = [type stringByReplacingOccurrencesOfString:@"Wind" withString:@"tuul"];
    type = [type stringByReplacingOccurrencesOfString:@"Humidity" withString:@"niiskus"];
    
    
    NSString *url = [[NSString alloc] initWithFormat:@"http://www.emhi.ee/ilma_andmed/input/graafik/%@_%@.png", city,type];
    NSString *urlWithLowerCase = [url lowercaseString];
    
    UIImage *graph = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:urlWithLowerCase]]];
    NSString *message = [[NSString alloc]initWithFormat:@"You are watching: %@ %@ graphic", city, type];
    UIAlertView *successAlert = [[UIAlertView alloc] initWithTitle:@"Graphics"
                                                           message:message
                                                          delegate:nil
                                                 cancelButtonTitle:@"OK"
                                                 otherButtonTitles:nil];
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(-20,-220,320,180)];
    [imageView setImage:graph];
    
    [successAlert addSubview:imageView];
     successAlert.transform = CGAffineTransformTranslate( successAlert.transform, 0.0, -150.0 );
    [successAlert show];
}

- (IBAction)backGraphics:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
