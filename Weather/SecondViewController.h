
#import <UIKit/UIKit.h>

@interface SecondViewController : UIViewController <UITableViewDataSource, UITableViewDelegate> {
    
}
- (IBAction)toPrognose:(id)sender;
- (IBAction)toWarnings:(id)sender;
- (IBAction)toFire:(id)sender;
- (IBAction)toSugavere:(id)sender;
- (IBAction)toHarku:(id)sender;
- (IBAction)toSatellite:(id)sender;

@end
