

#import "WeatherAppTests.h"
#import "DDXML.h"
#import "DDXMLElement.h"

@implementation WeatherAppTests

- (void)setUp
{
    [super setUp];
    
    // Set-up code here.
}

- (void)tearDown
{
    // Tear-down code here.
    
    [super tearDown];
}

//Checks that URL exists, element is <forecast> and attribute named "date" exists
-(void)testAttributeDate {
    NSArray *empty = [[NSArray alloc] init];
    NSString *date = [NSString stringWithContentsOfURL:[NSURL URLWithString:@"http://www.emhi.ee/ilma_andmed/xml/forecast.php?lang=eng"] encoding:NSISOLatin1StringEncoding error:nil];
    
    STAssertTrue(date != (NULL), @"check your URL");
    
    NSData *xmlData = [date dataUsingEncoding:NSUTF8StringEncoding];
    DDXMLDocument *xmlDoc = [[DDXMLDocument alloc] initWithData:xmlData options:0 error:nil];
    NSArray *resultNodes = nil;
    resultNodes = [xmlDoc nodesForXPath:@"//forecast" error:nil];
    
    STAssertTrue(resultNodes != empty, @"check your element name (not forecast)");
    
    for(DDXMLElement* resultElement in resultNodes){
        NSString *temp = [[resultElement attributeForName:@"date"] stringValue];

        STAssertTrue(temp != NULL, @"check your attribute name (not date)");
    }
}

//Checks that URL exists, element is <night> and another element is <text>
-(void)testElementNight {
    NSArray *empty = [[NSArray alloc] init];
    NSLog(@"ssssssssssssssss%@",empty);
    NSString *date = [NSString stringWithContentsOfURL:[NSURL URLWithString:@"http://www.emhi.ee/ilma_andmed/xml/forecast.php?lang=eng"] encoding:NSISOLatin1StringEncoding error:nil];
    
    STAssertTrue(date != (NULL), @"check your URL");
    
    NSLog(@"xxxxxxx%@",date);
    NSData *xmlData = [date dataUsingEncoding:NSUTF8StringEncoding];
    DDXMLDocument *xmlDoc = [[DDXMLDocument alloc] initWithData:xmlData options:0 error:nil];
    NSArray *getNight = [xmlDoc nodesForXPath:@"//night" error:nil];
    
    STAssertTrue(getNight != empty, @"check your element name (not night/text)");
    
    NSLog(@"aaaa%@",getNight);
    for(DDXMLElement *el in getNight){
        NSString *tt = [[[el elementsForName:@"text"] lastObject] stringValue];
        NSLog(@"aaaa%@",tt);
        
        STAssertTrue(tt != NULL, @"check your element name (not night/text)");
    }
}

//Checks that URL exists, element is <day> and another element is <text>
-(void)testElementDay {
    NSArray *empty = [[NSArray alloc] init];
    NSLog(@"ssssssssssssssss%@",empty);
    NSString *date = [NSString stringWithContentsOfURL:[NSURL URLWithString:@"http://www.emhi.ee/ilma_andmed/xml/forecast.php?lang=eng"] encoding:NSISOLatin1StringEncoding error:nil];
    
    STAssertTrue(date != (NULL), @"check your URL");
    
    NSData *xmlData = [date dataUsingEncoding:NSUTF8StringEncoding];
    DDXMLDocument *xmlDoc = [[DDXMLDocument alloc] initWithData:xmlData options:0 error:nil];
    NSArray *getDay = [xmlDoc nodesForXPath:@"//day" error:nil];
    
    STAssertTrue(getDay != empty, @"check your element name (not night/text)");
    
    for(DDXMLElement *el in getDay){
        NSString *tt = [[[el elementsForName:@"text"] lastObject] stringValue];
        
        STAssertTrue(tt != NULL, @"check your element name (not night/text)");
    }
}

-(void)testURL {
    NSString *date = [NSString stringWithContentsOfURL:[NSURL URLWithString:@"http://www.emhi.ee/ilma_andmed/xml/forecast.php?lang=eng"] encoding:NSISOLatin1StringEncoding error:nil];
    
    STAssertTrue(date != (NULL), @"check your URL");
}

@end
